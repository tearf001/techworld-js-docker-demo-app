## 演示项目 - developing with Docker

一个简单的应用页面,获取用户设置及修改
- 前端: index.html 纯HTML应用,无前端工程化
- 后端 nodejs + express
- 数据库 mongodb + mongodb-express 管理工具

## 分3步走:1部分docker托管.2全部托管,3.devops集成

### 后端使用容器
 后端数据库 mongo db 及 mongodb-express 管理网站

- 步骤 1: 创建docker 网络 (容器作为虚拟主机所在的LAN)

    ```docker network create mongo-network```

- 步骤 2: 启动 mongodb 

    ```bash
    docker run \
        -d \    # detach 脱离控制台执行
        -p 27017:27017 \    # 端口
        -e MONGO_INITDB_ROOT_USERNAME=admin \ # 初始根用户
        -e MONGO_INITDB_ROOT_PASSWORD=password \ # 初始密码
        --name mongodb \ # 名称!重要
        --net mongo-network \ #网络
        mongo # 镜像名称
    ```

- 步骤 3: 启动 mongo-express 数据库管理工具
    ``` bash
    docker run -d -p 8081:8081 \
        -e ME_CONFIG_MONGODB_ADMINUSERNAME=admin \
        -e ME_CONFIG_MONGODB_ADMINPASSWORD=password \        
        -e ME_CONFIG_MONGODB_SERVER=mongodb \
        --net mongo-network \
        --name mongo-express \
        mongo-express   
    ```
    > 注意网络是可选的
    _NOTE: creating docker-network in optional. You can start both containers in a default network. In this case, just emit `--net` flag in `docker run` command_

- 步骤 4[可选,观察mongodb]:  mongo-express 浏览器打开

    http://localhost:8081
    在mongo-express UI页面操作数据:
    - 1: create a new database "my-db"
    - 2: create a new collection "users" in the database "my-db"       
    > 实际上通过程序创建  (mongodb 是弱模型数据库)

- 步骤 5: 本地启动 nodejs applicatio - 在 `app` 项目目录
    ``` bash
    #安装依赖,主要是后端的express body-parser mongodb 驱动
    npm install 
    node server.js
    ```
- 步骤 6: 浏览器打开

    http://localhost:3000


### 项目自身使用Docker容器
 1.通过 [Dockfile](./Dockfile) 构建
 其逻辑同 传统开发,但注意一些配置略有不同.主要把连接改成mongodb
 `docker build -t myapp:1.0 .`
 2.至此.所有栈都容器化了.依次执行 
 ```bash
 # 1 运行mongdb 同之前
 docker run -d -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=password --name mongodb --net mongo-network mongo
 # 2 运行docker express 同之前
 docker run -d -p 8081:8081 -e ME_CONFIG_MONGODB_ADMINUSERNAME=admin -e ME_CONFIG_MONGODB_ADMINPASSWORD=password -e ME_CONFIG_MONGODB_SERVER=mongodb --net mongo-network --name mongo-express mongo-express
 # 3 运行项目
 docker run -d -p 3000:3000 myapp:1.0 # 如果之前指定网络,这里也必须指定
 ocker run -p3000:3000  --net mongo-network myapp:1.0

 ```

### 使用docker组合 Docker Compose
过程同上,区别:
- 3个构建描述文件写在一个文件中(docker-compose-yaml)
- 统一运行(根据依赖关系先后执行) 
    ```bash
    docker-compose -f docker-compose.yaml up # --build 如果项目没有构建,或者强制重建
    ```
- 统一清理 
    ```bash
    docker-compose -f docker-compose.yaml down # 关闭,清理容器,但不清理镜像
    ```

#### 手工创建 项目镜像
实践中不推荐,一般用于测试,单独推送
```bash
docker build -t my-app:1.0 .       # 在当前目录寻找Dockerfile进行构建
```    

# 数据卷
容器每次重启,或者删除都会毁掉容器上下文,因此数据持久化依赖于宿主host上的数据卷
```yaml
service:
    imagename
        ...
        volumes:
        # 实践中一般使用默认数据卷.而不用直接用路径
        - mongo-data:/data/db #如果未映射,在mongo容器中默认路径每次运行都重新生成.
# 默认路径为dbPath,也有教程指出为 /var/lib/mongodb,应该是错的
# 各个数据库默认位置不同, mysql: /var/lib/mysql; postgres: /var/lib/postgresql/data    
...    
volumes:
  mongo-data: # 命名数据卷
    driver: local # 根据数据库配置(一般是来自默认)
  其他命名卷:
    driver: /路径/

```
各平台默认的位置(如果为改动配置)
- windows: `c:\ProgramData\docker\volumns` docker 运行在window模式下
- linux: `/var/lib/docker/volumns` linux 或者 window docker in linux模式
    例如本应用在  `/var/lib/docker/volumns/techworld-js-docker-demo-app_mongo-data/_data`
- osx 同上
注意一点.linux 的位置是卷位置,而window和mac都是创建了Linux虚拟机的
比如 mac中 真实位置必须通过运行
`~/Library/Containers/com.docker.docker/Data/com.docker.driver.amd64-linux/tty`获得
在 `/var/lib/docker` 可以看到 columns, containers, images, network, plugins, swarm trust overlay2 tmp 等目录
columns 存有数据目录, 上面提及的命名卷的目录名比较友好 ..<appname>_mongo-data/_data,否则就是匿名目录