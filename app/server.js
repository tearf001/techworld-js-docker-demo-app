let express = require('express');
let path = require('path');
let fs = require('fs');
let MongoClient = require('mongodb').MongoClient;
let bodyParser = require('body-parser');
let app = express();

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

// 静态根文件 index.html
app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, "index.html"));
});
// 静态资源 图片
app.get('/profile-picture', function (req, res) {
  let img = fs.readFileSync(path.join(__dirname, "images/profile-1.jpg"));
  res.writeHead(200, { 'Content-Type': 'image/jpg' });
  res.end(img, 'binary');
});

// use when starting application locally 未进docker之前的开发方式,本地开发
let mongoUrlLocal = "mongodb://admin:password@localhost:27017";

// use when starting application as docker container 进docker之后的开发
let mongoUrlDocker = "mongodb://admin:password@mongodb";

let mongoconn = mongoUrlDocker
// pass these options to mongo client connect request to avoid DeprecationWarning for current Server Discovery and Monitoring engine
// mongodb的连接选项,避免警告,生产模式勿用
let mongoClientOptions = { useNewUrlParser: true, useUnifiedTopology: true };

// "user-account" in demo with docker. "my-db" in demo with docker-compose
let databaseName = "mydb"; // 数据库名称

app.post('/update-profile', function (req, res) {
  let userObj = req.body; //已经解析为json的对象 见第 11,9行

  MongoClient.connect(mongoconn, mongoClientOptions, function (err, client) {
    if (err) throw err;

    let db = client.db(databaseName);
    userObj['userid'] = 1; // 为了简化,限定为一个用户userid = 1

    let myquery = { userid: 1 };
    let newvalues = { $set: userObj };
    // update / insert
    db.collection("users").updateOne(myquery, newvalues, { upsert: true }, function (err, res) {
      if (err) throw err;
      client.close();
      console.warn('更新用户', new Date)
    });

  });
  // Send response
  res.send(userObj);
});
// 查询 ,限定 userid = 1
app.get('/get-profile', function (req, res) {
  let response = {};
  // Connect to the db
  MongoClient.connect(mongoconn, mongoClientOptions, function (err, client) {
    if (err) throw err;

    let db = client.db(databaseName);

    let myquery = { userid: 1 };

    db.collection("users").findOne(myquery, function (err, result) {
      if (err) throw err;
      response = result;
      console.warn('获取用户', new Date)
      client.close();
      res.send(response ? response : {});
    });
  });
});

app.listen(3000, function () {
  console.log("app listening on port 3000!");
});
